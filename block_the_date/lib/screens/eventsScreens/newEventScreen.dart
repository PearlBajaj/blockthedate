import 'package:block_the_date/components/constantColors.dart';
import 'package:block_the_date/components/constantTextStyles.dart';
import 'package:block_the_date/components/customButton.dart';
import 'package:block_the_date/logic/dateFuntions.dart';
import 'package:block_the_date/logic/models.dart';
import 'package:block_the_date/screens/eventsScreens/eventsScreen.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

Occurrence occurrence = Occurrence(false, false, false, false, false);


final dateformat = DateFormat.yMd();
DateTime date = DateTime.now();

final timeformat = DateFormat("hh:mm a");
DateTime time = DateTime.now();


class NewEventScreen extends StatefulWidget {
  @override
  _NewEventScreenState createState() => _NewEventScreenState();
}

class _NewEventScreenState extends State<NewEventScreen> {
  String occurrenceGetter(){
    if(occurrence.oneTime == true){
      return 'One Time Event';
    }
    else if(occurrence.weekly == true){
      return 'Weekly Event';
    }
    else if(occurrence.daily == true){
      return 'Daily Event';
    }
    else if(occurrence.monthly == true){
      return 'Monthly Event';
    }
    else if(occurrence.yearly == true){
      return 'Yearly Event';
    }
    return 'One Time Event';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize:
            Size.fromHeight(MediaQuery.of(context).size.height / 4.87),
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: Stack(
            children: <Widget>[
              Container(
                  width: MediaQuery.of(context).size.width * 100,
                  child: Image.asset(
                    'images/headerBanner.png',
                    fit: BoxFit.fill,
                  )),
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(top: 26.0, left: 8),
                  child: Container(
                    child: IconButton(
                      icon: Transform.scale(
                        scale: 1.25,
                        child: ImageIcon(
                          AssetImage(
                            'images/clear.png',
                          ),
                          color: appWhite,
                        ),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          PageRouteBuilder(
                            pageBuilder: (context, animation1, animation2) =>
                                EventScreen(),
                            transitionDuration: Duration(seconds: 0),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.only(top: 26.0, right: 16),
                  child: IconButton(
                    icon: Transform.scale(
                      scale: 1.25,
                      child: ImageIcon(
                        AssetImage('images/addImg.png'),
                        color: appWhite,
                        size: 24,
                      ),
                    ),
                    onPressed: () {},
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 16.0, right: 16),
                  child: IconButton(
                    icon: Transform.scale(
                        scale: 1.25,
                        child: ImageIcon(
                          AssetImage('images/edit.png'),
                          color: appWhite,
                          size: 24,
                        )),
                    onPressed: () {},
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 16.0, bottom: 16),
                  child: Text(
                    'Birthday Party',
                    style: boldTextStyle(24, appWhite),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: Padding(
                padding: const EdgeInsets.only(left: 16.0, right: 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      height: 24,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            height: 24,
                            width: 24,
                            child: Image.asset('images/time.png'),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 22.5,
                          ),
                          Text(
                            'Date & Time',
                            style: normalTextStyle(14, text3),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 56.0, right: 16),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Text(
                              'Mon, Oct 21, 2020',
                              style: normalTextStyle(14, text3),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Expanded(
                            child: Container(),
                          ),
                          Text(
                            '03:30 PM',
                            style: normalTextStyle(14, text3),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 56.0, right: 16),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Text(
                              'Mon, Oct 21, 2020',
                              style: normalTextStyle(14, text3),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Expanded(
                            child: Container(),
                          ),
                          Text(
                            '04:30 PM',
                            style: normalTextStyle(14, text3),
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        showModalBottomSheet<void>(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.vertical(
                                top: Radius.circular(24.0)),
                          ),
                          context: context,
                          builder: (BuildContext context) {
                            return EventOccurrence();
                          },
                        ).whenComplete(() {
                          setState(() {

                          });
                        });
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 24,
                            width: 24,
                            child: Image.asset('images/repeat.png'),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 22.5,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 1.25,
                            child: Text(
                              occurrenceGetter(),
                              style: normalTextStyle(14, text3),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        showModalBottomSheet<void>(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.vertical(
                                top: Radius.circular(24.0)),
                          ),
                          context: context,
                          builder: (BuildContext context) {
                            return ReminderSetter();
                          },
                        ).whenComplete((){
                          setState(() {
                            
                          });
                        });
                      },
                      child: Container(
                        height: 24,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: 24,
                              width: 24,
                              child: Image.asset('images/reminder.png'),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width / 22.5,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 2.2,
                              child: Text(
                                weekdayGetterFull(date.weekday)+ ' ' + monthGetter(date.month).substring(0,3)+ ' '+ date.day.toString()+', '+date.year.toString(),
                                style: normalTextStyle(14, text3),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            Expanded(
                              child: Container(),
                            ),
                            Text(
                             timeformat.format(time).toString(),
                              style: GoogleFonts.montserrat(
                                  fontSize: 14,
                                  color: Color(0xff333333),
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 1.0,
                      decoration: BoxDecoration(
                          color: Color(0xff707070),
                          borderRadius: BorderRadius.all(Radius.circular(24))),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 24,
                          width: 24,
                          child: Image.asset('images/email.png'),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 22.5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.25,
                          child: Text(
                            'user@gmail.com',
                            style: GoogleFonts.montserrat(
                                fontSize: 14,
                                color: Color(0xff333333),
                                fontWeight: FontWeight.w400),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 24,
                          width: 24,
                          child: Image.asset('images/timezone.png'),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 22.5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.25,
                          child: Text(
                            'Indian Standard Time',
                            style: normalTextStyle(14, text3),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: 1.0,
                      decoration: BoxDecoration(
                          color: Color(0xff707070),
                          borderRadius: BorderRadius.all(Radius.circular(24))),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 24,
                          width: 24,
                          child: Image.asset('images/addGuests.png'),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 22.5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.25,
                          child: Text(
                            'Add Guests',
                            style: normalTextStyle(14, textA),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 24,
                          width: 24,
                          child: Image.asset('images/location.png'),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 22.5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.25,
                          child: Text(
                            'Mumbai, Maharashtra, India',
                            style: normalTextStyle(14, text3),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 24,
                          width: 24,
                          child: Image.asset('images/description.png'),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 22.5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.25,
                          child: Text(
                            'Add Description ',
                            style: normalTextStyle(14, textA),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 24,
                          width: 24,
                          child: Image.asset('images/attachments.png'),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 22.5,
                        ),
                        Container(
                          height: 24,
                          width: MediaQuery.of(context).size.width / 2.8,
                          decoration: BoxDecoration(
                            color: appLilac,
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(2.0),
                            child: Stack(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        child: Image.asset(
                                          'images/img.png',
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Text(
                                        'IMG_7004568.jpg',
                                        style: normalTextStyle(10, text5),
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ],
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Icon(
                                    Icons.clear,
                                    size: 12,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: SaveButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
        ],
      ),
    );
  }
}

class EventOccurrence extends StatefulWidget {
  @override
  _EventOccurrenceState createState() => _EventOccurrenceState();
}

class _EventOccurrenceState extends State<EventOccurrence> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 2.5,
      child: StatefulBuilder(
        builder: (BuildContext context, StateSetter stateSetter) {
          return Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 4.0,
                  width: 40.0,
                  decoration: BoxDecoration(
                      color: Color(0xffCCCCCC),
                      borderRadius: BorderRadius.all(Radius.circular(24))),
                ),
                SizedBox(
                  height: 40,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 32.0),
                  child: Column(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          setState(() {
                            if (occurrence.oneTime == true) {
                              occurrence.oneTime = false;
                            } else {
                              occurrence.oneTime = true;
                              occurrence.daily = false;
                              occurrence.weekly = false;
                              occurrence.monthly = false;
                              occurrence.yearly = false;
                            }
                          });
                        },
                        child: CheckboxTileCircle(
                          value: occurrence.oneTime,
                          text: 'One Time Event',
                        ),
                      ),
                      SizedBox(
                        height: 22,
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            if (occurrence.daily == true) {
                              occurrence.daily = false;
                            } else {
                              occurrence.oneTime = false;
                              occurrence.daily = true;
                              occurrence.weekly = false;
                              occurrence.monthly = false;
                              occurrence.yearly = false;
                            }
                          });
                        },
                        child: CheckboxTileCircle(
                          value: occurrence.daily,
                          text: 'Daily',
                        ),
                      ),
                      SizedBox(
                        height: 22,
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            if (occurrence.weekly == true) {
                              occurrence.weekly = false;
                            } else {
                              occurrence.oneTime = false;
                              occurrence.daily = false;
                              occurrence.weekly = true;
                              occurrence.monthly = false;
                              occurrence.yearly = false;
                            }
                          });
                        },
                        child: CheckboxTileCircle(
                          value: occurrence.weekly,
                          text: 'Weekly',
                        ),
                      ),
                      SizedBox(
                        height: 22,
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            if (occurrence.monthly == true) {
                              occurrence.monthly = false;
                            } else {
                              occurrence.oneTime = false;
                              occurrence.daily = false;
                              occurrence.weekly = false;
                              occurrence.monthly = true;
                              occurrence.yearly = false;
                            }
                          });
                        },
                        child: CheckboxTileCircle(
                          value: occurrence.monthly,
                          text: 'Monthly',
                        ),
                      ),
                      SizedBox(
                        height: 22,
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            if (occurrence.yearly == true) {
                              occurrence.yearly = false;
                            } else {
                              occurrence.oneTime = false;
                              occurrence.daily = false;
                              occurrence.weekly = false;
                              occurrence.monthly = false;
                              occurrence.yearly = true;
                            }
                          });
                        },
                        child: CheckboxTileCircle(
                          value: occurrence.yearly,
                          text: 'Yearly',
                        ),
                      ),
                      SizedBox(
                        height: 22,
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}

class CheckboxTileCircle extends StatefulWidget {
  bool value;
  final String text;
  CheckboxTileCircle({this.value, this.text});
  @override
  _CheckboxTileCircleState createState() => _CheckboxTileCircleState();
}

class _CheckboxTileCircleState extends State<CheckboxTileCircle> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          height: 16,
          width: 16,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 1, color: textA),
          ),
          child: Padding(
            padding: const EdgeInsets.all(2.0),
            child: widget.value
                ? Container(
                    height: 10,
                    width: 10,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: appBlue,
                    ),
                  )
                : Container(
                    height: 10,
                    width: 10,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: appWhite,
                    ),
                  ),
          ),
        ),
        SizedBox(
          width: 16,
        ),
        Text(
          widget.text,
          style: normalTextStyle(14, text3),
        ),
      ],
    );
  }
}

class ReminderSetter extends StatefulWidget {
  @override
  _ReminderSetterState createState() => _ReminderSetterState();
}

class _ReminderSetterState extends State<ReminderSetter> {
  bool isSwitched = false;
  TextEditingController timeController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  @override
  initState() {
    timeController = new TextEditingController();
    timeController.text = '06:30 PM';
    dateController = new TextEditingController();
    dateController.text = '24/05/2020';
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height / 3.1,
        child: StatefulBuilder(
          builder: (BuildContext context, StateSetter stateSetter) {
            return Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    height: 4.0,
                    width: 40.0,
                    decoration: BoxDecoration(
                        color: Color(0xffCCCCCC),
                        borderRadius: BorderRadius.all(Radius.circular(24))),
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 16.0),
                        child: ListTile(
                          title: Text(
                            'No Reminders',
                            style: GoogleFonts.montserrat(
                                fontSize: 14,
                                color: Color(0xff333333),
                                fontWeight: FontWeight.w400),
                          ),
                          trailing: Switch(
                            value: isSwitched,
                            onChanged: (value) {
                              setState(() {
                                isSwitched = value;
                                print(isSwitched);
                              });
                            },
                            activeTrackColor: appBlue.withOpacity(0.5),
                            activeColor: appBlue,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 8.0, bottom: 8),
                                child: Text(
                                  'Daily',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 14,
                                      color: Color(0xff333333),
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 2.5,
                                height: 40,
                                child: DateTimeField(
                                  onChanged: (value) {
                                    date = value;
                                  },
                                  format: dateformat,
                                  decoration: InputDecoration(
                                    hintText: 'Date',
                                    suffixIcon: Transform.scale(
                                        scale: 0.75,
                                        child: ImageIcon(
                                          AssetImage('images/calendar.png'),
                                          color: appBlue,
                                        )),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                  ),
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                  onShowPicker: (context, currentValue) {
                                    return showDatePicker(
                                        context: context,
                                        firstDate: DateTime(1900),
                                        initialDate:
                                        currentValue ?? DateTime.now(),
                                        lastDate: DateTime(2100));
                                  },
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 8.0, bottom: 8),
                                child: Text(
                                  'Daily',
                                  style: GoogleFonts.montserrat(
                                      fontSize: 14,
                                      color: Color(0xff333333),
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 2.5,
                                height: 40,
                                child: DateTimeField(
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                  decoration: InputDecoration(
                                    hintText: 'Time',
                                    suffixIcon: Transform.scale(
                                        scale: 0.75,
                                        child: ImageIcon(
                                          AssetImage('images/time.png'),
                                          color: appBlue,
                                        )),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                  ),
                                  onChanged: (value) {
                                    time = value;
                                  },
                                  format: timeformat,
                                  onShowPicker: (context, currentValue) async {
                                    final time = await showTimePicker(
                                      context: context,
                                      initialTime: TimeOfDay.fromDateTime(
                                          currentValue ?? DateTime.now()),
                                    );
                                    return DateTimeField.convert(time);
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 56,
                      ),
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
