import 'package:block_the_date/screens/homeScreens/eventListScreen.dart';
import 'package:block_the_date/screens/homeScreens/homeScreen.dart';
import 'package:block_the_date/screens/homeScreens/notificationScreen.dart';
import 'package:block_the_date/screens/loginScreen.dart';
import 'package:block_the_date/screens/shopScreens/shopList.dart';
import 'package:block_the_date/screens/splashScreen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: appTheme),
      home: SplashScreen(),
      routes: <String, WidgetBuilder>{
        'Login': (BuildContext context) => LoginScreen(),
        'Home': (BuildContext context) => HomeScreen(),
        'ShopList': (BuildContext context) => ShopsList(),
        'Notification': (BuildContext context) => NotificationScreen(),
        'EventList': (BuildContext context) => EventListScreen(),
      },
    );
  }
}

const MaterialColor appTheme = const MaterialColor(
  0xff4F59F8,
  const <int, Color>{
    50: Color(0xff4F59F8),
    100: Color(0xff4F59F8),
    200: Color(0xff4F59F8),
    300: Color(0xff4F59F8),
    400: Color(0xff4F59F8),
    500: Color(0xff4F59F8),
    600: Color(0xff4F59F8),
    700: Color(0xff4F59F8),
    800: Color(0xff4F59F8),
    900: Color(0xff4F59F8),
  },
);
